﻿#include"MatrixV2.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

int InputCheck() {
	int number = 0;
	std::cin >> number;
	while (std::cin.fail()) {    
		std::cin.clear();     // перед следующей попыткой ввода надо снять флаг ошибки функцией 
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // и удалить все находящиеся в нем данные
		std::cout << "Incorrect! Repeat Input:";
		std::cin >> number;
	}
	return number;
}


Matrix* InputMatrix() {

	Matrix* matrix = CreateMatrix();

	int column = 0;
	int row = 0;
	int value = 0;
	int size = 0;

	std::cout << "Number of lines:";
	matrix->rows = InputCheck();


	std::cout << "Number of columns:";
	matrix->columns = InputCheck();

	std::cout << "Number of not zero elements:";
	size = InputCheck();

	while (matrix->columns <= 0 || matrix->rows <=0 || size<0 || size > matrix->rows * matrix->columns) {
		std::cout << "Invalid input!\n";

		std::cout << "Number of lines:";
		matrix->rows = InputCheck();


		std::cout << "Number of columns:";
		matrix->columns = InputCheck();

		std::cout << "Number of not zero elements:";
		size = InputCheck();
	}
	
	for (int i = 0; i < size; i++) {
		
		std::cout << "line:";
		row = InputCheck();

		std::cout << "column:";
		column = InputCheck();

		std::cout << "value:";
		value = InputCheck();

		while (row <= 0 || row > matrix->rows || column <= 0 || column > matrix->columns) {

			std::cout << "Invalid input!\n";

			std::cout << "line:";
			row = InputCheck();

			std::cout << "column:";
			column = InputCheck();

			std::cout << "value:";
			value = InputCheck();
		}

		AddNode(matrix, row-1, column-1, value);
	}
	matrix->rows -= 1;
	matrix->columns -= 1;
	return matrix;
}



int main()
{
	/*Matrix* matrix = CreateMatrix();
	matrix->rows = 3 - 1;
	matrix->columns = 3 - 1;
	AddNode(matrix, 1 - 1 ,1 - 1,232);
	AddNode(matrix, 2 - 1, 2 - 1,777);
	AddNode(matrix, 3 - 1, 3 - 1,44);
	PrintMatrix(matrix);
	std::cout << ":\n";
	Matrix* new_matrix = nullptr;
	new_matrix = ChangeMatrix(matrix, 1, typemore);
	PrintMatrix(new_matrix);
	std::cout << ":\n:";
	DestroyMatrix(new_matrix);
	new_matrix = ChangeMatrix(matrix, 3, typeless);
	PrintMatrix(new_matrix);
	std::cout << ":\n:";
	DestroyMatrix(new_matrix);
	new_matrix = ChangeMatrix(matrix, 2, typediv);
	PrintMatrix(new_matrix);
	std::cout << ":\n:";
	DestroyMatrix(new_matrix);*/


	Matrix* matrix = InputMatrix();
	PrintMatrix(matrix);

	int choise = 0;
	int criterion = 0;
	Matrix* new_matrix = nullptr;

	while (1) {

		std::cout << "1. digits more then _\n2. digits less then_\n3. digits multiplies_\n0. exit\n";
		std::cin >> choise;

		if (choise == 1) {
			std::cout << "Criterion:";
			criterion = InputCheck();
			new_matrix = ChangeMatrix(matrix, criterion, typemore);
			PrintMatrix(new_matrix);
			DestroyMatrix(new_matrix);
		}
		else if (choise == 2) {
			std::cout << "Criterion:";
			criterion = InputCheck();
			new_matrix = ChangeMatrix(matrix, criterion, typeless);
			PrintMatrix(new_matrix);
			DestroyMatrix(new_matrix);
		}
		else if (choise == 3) {
			std::cout << "Criterion:";
			criterion = InputCheck();
			if (criterion == 0) {
				std::cout << "Don't divide by zero :)\n";
				continue;
			}
			new_matrix = ChangeMatrix(matrix, criterion, typediv);
			PrintMatrix(new_matrix);
			DestroyMatrix(new_matrix);
		}
		else if (choise == 0) {
			break;
		}
		else {
			std::cout << "\n";
			continue;
		}
	}
	DestroyMatrix(matrix);

	_CrtDumpMemoryLeaks();
}


