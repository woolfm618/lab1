#include "MatrixV2.h"


Matrix* CreateMatrix() {
	Matrix* matrix = new Matrix();
	return matrix;
}

void AddRow(Matrix* matrix, int row) {

	if (matrix->first_row == nullptr) {
		matrix->first_row = new Row();
		matrix->first_row->row_index = row;
		return;
	}

	Row* target_row = matrix->first_row;
	Row* tmp = nullptr;

	if (row < target_row->row_index) { // �������� ��� ������� ����
		tmp = new Row{ nullptr,row,target_row};
		/*tmp->row_index = row;
		tmp->next_row = target_row*/;
		matrix->first_row = tmp;
		return;
	}

	if (row == target_row->row_index) {
		return;
	}

	while (1) {                        // ����� ������
		if (target_row->next_row == nullptr) {
			target_row->next_row = new Row();
			target_row->next_row->row_index = row;
			return;
		}
		else if (row > target_row->next_row->row_index) {
			target_row = target_row->next_row;
		}
		else if (row == target_row->next_row->row_index) {
			return;
		}
		else if (row < target_row->next_row->row_index) {
			tmp = new Row();
			tmp->row_index = row;
			tmp->next_row = target_row->next_row;
			target_row->next_row = tmp;
			matrix->rows += 1;
			return;
		}
	}
}

Row* FindRow(Matrix* matrix, int row) {

	Row* target_row = matrix->first_row;

	if (target_row == nullptr) {
		return nullptr;
	}

	while (1) {
		if (target_row->row_index == row) {
			return target_row;
		}
		if (target_row->next_row != nullptr) {
			target_row = target_row->next_row;
		}
		else {
			return nullptr;
		}
	}
}

void AddNode(Matrix* matrix, int row, int column, int value) {

	Row* target_row = nullptr;

	if (FindRow(matrix, row) == nullptr) {
		AddRow(matrix, row);
	}

	target_row = FindRow(matrix, row);


	if (target_row->first_node == nullptr ) {
		target_row->first_node = new Node{row,column,value,nullptr};
		return;
	}
	
	Node* target_node = target_row->first_node;
	Node* tmp = nullptr;

	if (target_row->first_node->column > column) {
		tmp = new Node{row,column,value,target_node};
		target_row->first_node = tmp;
		return;
	}

	if (column == target_node->column) {
		return;
	}

	while (1) {                        
		if (target_node->next_node == nullptr) {
			target_node->next_node = new Node{ row,column,value,nullptr };
			return;
		}
		else if (column > target_node->next_node->column) {
			target_node = target_node->next_node;
		}
		else if (column == target_node->next_node->column) {
			return;
		}
		else if (column < target_node->next_node->column) {
			tmp = new Node{ row,column,value,target_node->next_node };
			target_node->next_node = tmp;
			return;
		}
	}
}

void PrintMatrix(Matrix* matrix) {

	Row* target_row = matrix->first_row;


	if (target_row == nullptr) {
		for (int i = 0; i < matrix->rows + 1; i++) {
			for (int j = 0; j < matrix->columns + 1; j++) {
				std::cout << "0 ";
			}
			std::cout << "\n";
		}
		return;
	}

	Node* target_node = target_row->first_node;

	if (target_row->row_index > 0) {   // ������� ������ �� ������ ������, >1 ��� ������ � 1
		for (int i = 0; i < target_row->row_index; i++) {
			for (int j = 0; j < matrix->columns + 1; j++) {
				std::cout << "0 ";
			}
			std::cout << "\n";
		}
	}

	while (1) {

		if (target_row->first_node->column > 0) { // ������� �������� �� ������� �������� � ������ 
			for (int j = 0; j < target_row->first_node->column; j++) { // -1 ��� ������ � 1
				std::cout << "0 ";
			}
		}
		while (1) {
			std::cout << target_node->value << " "; // ����� ��������
			
			if (target_node->next_node == nullptr) {  // ������� �������� ����� ����������, ����� ������ 
				for (int i = 0; i < (matrix->columns - target_node->column); i++) {
					std::cout << "0 ";
				}
				break;
			}

			else if (target_node->next_node->column > target_node->column) { // ������� �������� ����� ����������
				for (int i = 0; i < target_node->next_node->column - target_node->column - 1; i++) {
					std::cout << "0 ";
				}
				target_node = target_node->next_node;
			}

		}
		if (target_row->next_row != nullptr) {
			if (target_row->next_row->row_index > target_row->row_index + 1) { // ������� ������ ����� �������� + ������� �� ���� ������
				std::cout << "\n";
				for (int i = 0; i < target_row->next_row->row_index - target_row->row_index - 1; i++) {
					for (int j = 0; j < matrix->columns + 1; j++) {
						std::cout << "0 ";
					}
					if (i != target_row->next_row->row_index - target_row->row_index - 2) { // ���� ���� ������ �� ������� ������ ��� �� ���� ������
						std::cout << "\n";
					}
				}
			}
			target_row = target_row->next_row; //  ����� ������ 
			target_node = target_row->first_node;
			std::cout << "\n";
			continue;
		}

		else if (target_row->next_row == nullptr) {  // ������� ������ ����� ���������, ����� ������
			std::cout << "\n";
			for (int i = 0; i < matrix->rows - target_row->row_index; i++) {// -1 ��� ������ � 1
				for (int j = 0; j < matrix->columns + 1; j++) {
					std::cout << "0 ";
				}
				std::cout << "\n";
			}
			break;
		}
	}
}

bool PassCriterion(Node* node, int criterion, int type) {
	int tmp = node->value;
	int pass_flag = 0;
	while (tmp != 0) {
		if (type == 1 && (tmp % 10) <= criterion) {
			pass_flag += 1;
		}
		if (type == 2 && (tmp % 10) >= criterion) {
			pass_flag += 1;
		}
		if (type == 3 && (tmp % 10) % criterion != 0) {
			pass_flag += 1;
		}
		tmp = tmp / 10;
	}
	if (pass_flag > 0) {
		return false;
	}
	return true;
}

Matrix* ChangeMatrix(Matrix* matrix, int criterion, int type) {

	Matrix* new_matrix = new Matrix{matrix->rows, matrix->columns, nullptr};

	Row* target_row = matrix->first_row;

	if (target_row == nullptr) { // ������������
		return matrix;
	}

	if (target_row->first_node == nullptr && target_row->next_row == nullptr) {
		return matrix;
	}

	Node* target_node = target_row->first_node;




	while (1) {

		while (1) {
			if (PassCriterion(target_node, criterion, type)) {
				AddNode(new_matrix, target_node->row, target_node->column, target_node->value);
			}
			if (target_node->next_node == nullptr) {
				break;
			}
			else {
				target_node = target_node->next_node;
			}
		}
		if (target_row->next_row == nullptr) {
			break;
		}
		else {
			target_row = target_row->next_row;
			target_node = target_row->first_node;
		}
	}

	return new_matrix;
}

void DestroyMatrix(Matrix* matrix) {

	if (matrix == nullptr) {
		return;
	}

	if (matrix->first_row == nullptr) {
		delete matrix;
		return;
	}

	Row* target_row = matrix->first_row;
	Node* target_node = matrix->first_row->first_node;
	Row* tmp_row = nullptr;
	Node* tmp_node = nullptr;

	while (target_row) {
		while (target_node) {
			tmp_node = target_node->next_node;
			delete target_node;
			target_node = tmp_node;
		}
		tmp_row = target_row->next_row;
		delete target_row;
		target_row = tmp_row;
		if (target_row != nullptr) {
			target_node = target_row->first_node;
		}
	}
	delete matrix;
}