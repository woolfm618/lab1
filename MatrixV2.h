#pragma once
#include <iostream>

#define typemore 1
#define typeless 2
#define typediv 3

typedef struct Node {
	int row;
	int column;
	int value;
	Node* next_node;
}Node;


typedef struct Row {
	Node* first_node;
	int row_index;
	Row* next_row;
}Row;


typedef struct Matrix {
	int rows;
	int columns;
	Row* first_row; // ������ ����� �� �������
}Matrix;

Matrix* CreateMatrix();
void DestroyMatrix(Matrix* matrix);

void AddNode(Matrix* matrix, int row, int column, int value);

void PrintMatrix(Matrix* matrix);

Matrix* ChangeMatrix(Matrix* matrix, int criterion,int type);
bool PassCriterion(Node* node, int criterion, int type);
